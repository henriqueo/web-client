export const getAppDetails = () => {
    return JSON.parse(localStorage.getItem('appDetails'))
}

export const getAccessToken = () => {
    return JSON.parse(localStorage.getItem('accessToken'))
}

export const isUrl = (url) => {
    return url.match(
        /^[a-zA-Z]+\:\/\/\w+\.\w+\w$|^[a-zA-z]+\:\/\/\w+\:\d+$/) != null
}

export const APIRequest = async (url, headers, body, method) => {
    const app = getAppDetails()

    const accessToken = getAccessToken()

    const APIRoot = `/api/v1/`

    if (!isUrl(url))
        url = `${app.instance_url}${APIRoot}${url}`

    headers = (typeof headers !== 'undefined') ? headers : {}

    if (accessToken != null)
        headers['Authorization'] = (`Bearer ${accessToken.access_token}`)

    body = (typeof body !== 'undefined') ? body : {}

    method = (typeof method !== 'undefined') ? method.toUpperCase(): 'GET'

    const req = { method, headers }

    if (method.toLowerCase() == 'post' || method.toLowerCase() == 'put')
        req['body'] = body

    const res = await fetch(url, req)

    return await res.json()
}

