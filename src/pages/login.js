import { redirect } from '@roxi/routify'
import { APIRequest, getAppDetails, getAccessToken } from './utils.js'

export const registerApp = async (instanceUrl, redirectUri) => {
    const appUrl = `${instanceUrl}/api/v1/apps/`

    const headers = { 'Content-type':'application/json' }

    const body = JSON.stringify({
         'name':'web-client',
         'redirect_uris':redirectUri,
         'authorization_grant_type': 'authorization-code',
         'client_type': 'confidential'
    })

    const res = await fetch(appUrl, {
       method: 'POST',
       headers,
       body
    })

    const app = await res.json()

    app['instance_url'] = instanceUrl

    localStorage.setItem('appDetails', JSON.stringify(app))
}

export const generateAuthorizationUrl = async () => {
    const app = getAppDetails()

    return (
        `${app.instance_url}/o/authorize?` +
        `client_id=${app.client_id}&` +
        `response_type=code&` +
        `redirectUri=${app.redirect_uri}`)
}

export const getUser = async () => {
    return await APIRequest(`users/verify_credentials`)
}

export const generateToken = async (authorizationCode) => {
    const app = getAppDetails()

    const tokenGenUrl = `${app.instance_url}/o/token/`

    const data = {
        'grant_type': 'authorization_code',
        'code': authorizationCode,
        'client_id': app.client_id,
        'client_secret': app.client_secret
    }

    const form = new URLSearchParams(data).toString()

    const res = await fetch(tokenGenUrl, {
        'method': 'POST',
        'body': form,
        'headers': {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })

    localStorage.setItem('accessToken', JSON.stringify(await res.json()))
}

export const logOut = async () => {
    const app = getAppDetails()

    const accessToken = getAccessToken()

    const revokeTokenUrl = `${app.instance_url}/o/revoke_token/`

    const data = {
        'token': accessToken.access_token,
        'client_id': app.client_id,
        'client_secret': app.client_secret
    }

    const form = new URLSearchParams(data).toString()

    const res = await fetch(revokeTokenUrl, {
        'method': 'POST',
        'body': form,
        'headers': {
            'Authorization': `Bearer ${accessToken.access_token}`,
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })

    if (res.status == 200)
        localStorage.clear()
}


export const auth = async () => {
    const user = await getUser()[0]
    if (user == undefined)
        return false
    return user
}
