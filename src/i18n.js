import { addMessages, init, getLocaleFromNavigator } from 'svelte-i18n';

import en from './i18n/en.json';
import enUS from './i18n/en-US.json';

import navPt from './i18n/pt/nav.json';
import libraryPt from './i18n/pt/library.json';

addMessages('en', en);
addMessages('en-US', enUS);
addMessages('pt', navPt);
addMessages('pt', libraryPt);

init({
  fallbackLocale: 'en',
  initialLocale: getLocaleFromNavigator(),
});
