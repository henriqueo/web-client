
/**
 * @roxi/routify 2.7.3
 * File generated Sun Jan 03 2021 18:33:20 GMT-0300 (Brasilia Standard Time)
 */

export const __version = "2.7.3"
export const __timestamp = "2021-01-03T21:33:20.324Z"

//buildRoutes
import { buildClientTree } from "@roxi/routify/runtime/buildRoutes"

//imports


//options
export const options = {}

//tree
export const _tree = {
  "name": "_layout",
  "filepath": "/_layout.svelte",
  "root": true,
  "ownMeta": {
    "preload": "proximity"
  },
  "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/_layout.svelte",
  "children": [
    {
      "isFile": true,
      "isDir": false,
      "file": "_fallback.svelte",
      "filepath": "/_fallback.svelte",
      "name": "_fallback",
      "ext": "svelte",
      "badExt": false,
      "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/_fallback.svelte",
      "importPath": "../src/pages/_fallback.svelte",
      "isLayout": false,
      "isReset": false,
      "isIndex": false,
      "isFallback": true,
      "isPage": false,
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": "proximity",
        "prerender": true
      },
      "path": "/_fallback",
      "id": "__fallback",
      "component": () => import('../src/pages/_fallback.svelte').then(m => m.default)
    },
    {
      "isFile": true,
      "isDir": true,
      "file": "_layout.svelte",
      "filepath": "/web/_layout.svelte",
      "name": "_layout",
      "ext": "svelte",
      "badExt": false,
      "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/web/_layout.svelte",
      "children": [
        {
          "isFile": true,
          "isDir": true,
          "file": "_layout.svelte",
          "filepath": "/web/library/_layout.svelte",
          "name": "_layout",
          "ext": "svelte",
          "badExt": false,
          "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/web/library/_layout.svelte",
          "children": [
            {
              "isFile": false,
              "isDir": true,
              "file": "document",
              "filepath": "/web/library/document",
              "name": "document",
              "ext": "",
              "badExt": false,
              "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/web/library/document",
              "children": [
                {
                  "isFile": true,
                  "isDir": false,
                  "file": "[document].svelte",
                  "filepath": "/web/library/document/[document].svelte",
                  "name": "[document]",
                  "ext": "svelte",
                  "badExt": false,
                  "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/web/library/document/[document].svelte",
                  "importPath": "../src/pages/web/library/document/[document].svelte",
                  "isLayout": false,
                  "isReset": false,
                  "isIndex": false,
                  "isFallback": false,
                  "isPage": true,
                  "ownMeta": {},
                  "meta": {
                    "recursive": true,
                    "preload": "proximity",
                    "prerender": true
                  },
                  "path": "/web/library/document/:document",
                  "id": "_web_library_document__document",
                  "component": () => import('../src/pages/web/library/document/[document].svelte').then(m => m.default)
                }
              ],
              "isLayout": false,
              "isReset": false,
              "isIndex": false,
              "isFallback": false,
              "isPage": false,
              "ownMeta": {},
              "meta": {
                "recursive": true,
                "preload": "proximity",
                "prerender": true
              },
              "path": "/web/library/document"
            },
            {
              "isFile": false,
              "isDir": true,
              "file": "folder",
              "filepath": "/web/library/folder",
              "name": "folder",
              "ext": "",
              "badExt": false,
              "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/web/library/folder",
              "children": [
                {
                  "isFile": true,
                  "isDir": false,
                  "file": "[folder].svelte",
                  "filepath": "/web/library/folder/[folder].svelte",
                  "name": "[folder]",
                  "ext": "svelte",
                  "badExt": false,
                  "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/web/library/folder/[folder].svelte",
                  "importPath": "../src/pages/web/library/folder/[folder].svelte",
                  "isLayout": false,
                  "isReset": false,
                  "isIndex": false,
                  "isFallback": false,
                  "isPage": true,
                  "ownMeta": {},
                  "meta": {
                    "recursive": true,
                    "preload": "proximity",
                    "prerender": true
                  },
                  "path": "/web/library/folder/:folder",
                  "id": "_web_library_folder__folder",
                  "component": () => import('../src/pages/web/library/folder/[folder].svelte').then(m => m.default)
                }
              ],
              "isLayout": false,
              "isReset": false,
              "isIndex": false,
              "isFallback": false,
              "isPage": false,
              "ownMeta": {},
              "meta": {
                "recursive": true,
                "preload": "proximity",
                "prerender": true
              },
              "path": "/web/library/folder"
            },
            {
              "isFile": true,
              "isDir": false,
              "file": "index.svelte",
              "filepath": "/web/library/index.svelte",
              "name": "index",
              "ext": "svelte",
              "badExt": false,
              "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/web/library/index.svelte",
              "importPath": "../src/pages/web/library/index.svelte",
              "isLayout": false,
              "isReset": false,
              "isIndex": true,
              "isFallback": false,
              "isPage": true,
              "ownMeta": {},
              "meta": {
                "recursive": true,
                "preload": "proximity",
                "prerender": true
              },
              "path": "/web/library/index",
              "id": "_web_library_index",
              "component": () => import('../src/pages/web/library/index.svelte').then(m => m.default)
            }
          ],
          "isLayout": true,
          "isReset": false,
          "isIndex": false,
          "isFallback": false,
          "isPage": false,
          "importPath": "../src/pages/web/library/_layout.svelte",
          "ownMeta": {
            "icon": "topic"
          },
          "meta": {
            "icon": "topic",
            "recursive": true,
            "preload": "proximity",
            "prerender": true
          },
          "path": "/web/library",
          "id": "_web_library__layout",
          "component": () => import('../src/pages/web/library/_layout.svelte').then(m => m.default)
        },
        {
          "isFile": true,
          "isDir": true,
          "file": "_layout.svelte",
          "filepath": "/web/trash/_layout.svelte",
          "name": "_layout",
          "ext": "svelte",
          "badExt": false,
          "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/web/trash/_layout.svelte",
          "children": [
            {
              "isFile": true,
              "isDir": false,
              "file": "index.svelte",
              "filepath": "/web/trash/index.svelte",
              "name": "index",
              "ext": "svelte",
              "badExt": false,
              "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/web/trash/index.svelte",
              "importPath": "../src/pages/web/trash/index.svelte",
              "isLayout": false,
              "isReset": false,
              "isIndex": true,
              "isFallback": false,
              "isPage": true,
              "ownMeta": {},
              "meta": {
                "recursive": true,
                "preload": "proximity",
                "prerender": true
              },
              "path": "/web/trash/index",
              "id": "_web_trash_index",
              "component": () => import('../src/pages/web/trash/index.svelte').then(m => m.default)
            }
          ],
          "isLayout": true,
          "isReset": false,
          "isIndex": false,
          "isFallback": false,
          "isPage": false,
          "importPath": "../src/pages/web/trash/_layout.svelte",
          "ownMeta": {
            "icon": "delete"
          },
          "meta": {
            "icon": "delete",
            "recursive": true,
            "preload": "proximity",
            "prerender": true
          },
          "path": "/web/trash",
          "id": "_web_trash__layout",
          "component": () => import('../src/pages/web/trash/_layout.svelte').then(m => m.default)
        },
        {
          "isFile": true,
          "isDir": false,
          "file": "index.svelte",
          "filepath": "/web/index.svelte",
          "name": "index",
          "ext": "svelte",
          "badExt": false,
          "absolutePath": "/home/henrique/Projects/bibliotec/web-client/src/pages/web/index.svelte",
          "importPath": "../src/pages/web/index.svelte",
          "isLayout": false,
          "isReset": false,
          "isIndex": true,
          "isFallback": false,
          "isPage": true,
          "ownMeta": {
            "title": "Home"
          },
          "meta": {
            "title": "Home",
            "recursive": true,
            "preload": "proximity",
            "prerender": true
          },
          "path": "/web/index",
          "id": "_web_index",
          "component": () => import('../src/pages/web/index.svelte').then(m => m.default)
        }
      ],
      "isLayout": true,
      "isReset": false,
      "isIndex": false,
      "isFallback": false,
      "isPage": false,
      "importPath": "../src/pages/web/_layout.svelte",
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": "proximity",
        "prerender": true
      },
      "path": "/web",
      "id": "_web__layout",
      "component": () => import('../src/pages/web/_layout.svelte').then(m => m.default)
    }
  ],
  "isLayout": true,
  "isReset": false,
  "isIndex": false,
  "isFallback": false,
  "isPage": false,
  "isFile": true,
  "file": "_layout.svelte",
  "ext": "svelte",
  "badExt": false,
  "importPath": "../src/pages/_layout.svelte",
  "meta": {
    "preload": "proximity",
    "recursive": true,
    "prerender": true
  },
  "path": "/",
  "id": "__layout",
  "component": () => import('../src/pages/_layout.svelte').then(m => m.default)
}


export const {tree, routes} = buildClientTree(_tree)

